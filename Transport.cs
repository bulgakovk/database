﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database
{
    public abstract class Transport
    {
        private Driver _driver;

        abstract protected void Drive ();

        abstract protected void Repare ();

        public Driver driver
        {
            get
            {
                return _driver;
            }

            set
            {
                _driver = value;
            }
        }

        protected bool TryToRepare ()
        {
            this.Repare();
            Random randomGenerator = new Random();
            return randomGenerator.Next(0, 1) == 1;
        }
    }
}
