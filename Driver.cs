﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database
{
    public class Driver
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }

        }

        public Driver (string name)
        {
            this._name = name;
        }
    }
}
