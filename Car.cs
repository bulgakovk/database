﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database
{
    public class Car : Transport
    {
        public Car (Driver driver)
        {
            this.driver = driver;
        }
        
        protected override void Drive ()
        {
            Console.WriteLine("Car is driving");
        }

        protected override void Repare ()
        {
            ChangeWheels();
        }

        private void ChangeWheels ()
        {
            Console.WriteLine("Trying to fix wheels!");
        }
    }
}
