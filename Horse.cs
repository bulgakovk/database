﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database
{
    public class Horse : Transport
    {
        public Horse (Driver driver)
        {
            this.driver = driver;
        }

        protected override void Drive ()
        {
            Console.WriteLine("Horse is riding");
        }

        protected override void Repare()
        {
            resurrectHorse();
        }

        private void resurrectHorse ()
        {
            Console.WriteLine("WTF I'm doing?");
        }
    }
}
