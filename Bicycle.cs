﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database
{
    public class Bicycle : Transport
    {
        public Bicycle (Driver driver)
        {
            this.driver = driver;
        }

        protected override void Drive ()
        {
            Console.WriteLine("Bicycle is driving");
        }

        protected override void Repare ()
        {
            ChangeChain();
        }

        private void ChangeChain ()
        {
            Console.WriteLine("Trying to fix chain!");
        }
    }
}
